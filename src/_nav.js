export default {
  items: [
    {
      name: 'Users',
      url: '/',
      icon: 'icon-speedometer',
    },
    {
      name: 'Transactions',
      url: '/transactions',
      icon: 'icon-list',
    },
  ]
}
